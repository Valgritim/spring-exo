package org.eclipse.model;

public class Order {

	private int id;
	private int orderNumber;
	private String amount;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getOrderNumber() {
		return orderNumber;
	}
	public void setOrderNumber(int orderNumber) {
		this.orderNumber = orderNumber;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	
	@Override
	public String toString() {
		return "Order [id=" + id + ", orderNumber=" + orderNumber + ", amount=" + amount + "]";
	}
	
	
}

package org.eclipse.model;

import java.util.List;

public class User {

	private int id;
	private String firstname;
	private String lastname;
	private Order order;
	private List<String> paymentMethods;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public Order getOrder() {
		return order;
	}
	public void setOrder(Order order) {
		this.order = order;
	}
	public List<String> getPaymentMethods() {
		return paymentMethods;
	}
	public void setPaymentMethods(List<String> paymentMethods) {
		this.paymentMethods = paymentMethods;
	}
	
	public void afficher() {
		System.out.println("User [id=" + id + ", firstname=" + firstname + ", lastname=" + lastname + ", order=" + order
				+ "]");
		System.out.println("Les moyens de paiement de " + firstname);
		paymentMethods.forEach(System.out::println);
	}
	
	
}

package org.eclipse.main;


import org.eclipse.model.User;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
    	ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        User u = context.getBean("user", User.class);
        u.afficher();
        
        System.out.println("------------------------------------------------------------------------");
        
    }
}
